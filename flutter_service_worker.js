'use strict';
const MANIFEST = 'flutter-app-manifest';
const TEMP = 'flutter-temp-cache';
const CACHE_NAME = 'flutter-app-cache';
const RESOURCES = {
  "assets/AssetManifest.json": "77a4918d7c9cd2ef6967079271d2c9a3",
"assets/assets/animation/lottie_delete.json": "691b0d9aae5c30e4ca268082a8a32be3",
"assets/assets/animation/lottie_delivery.json": "906fcf48a121726057e5fb3b9c67a826",
"assets/assets/animation/lottie_guideHint.json": "062c551b77cb90c0abd4ecfa25fbd5ea",
"assets/assets/animation/lottie_invoice.json": "41f8f1996533cf403105ba59a4465092",
"assets/assets/animation/lottie_sendPrescriptionGuide.json": "8eacbaf7c27e528483a47e288884b15d",
"assets/assets/fonts/font-icons.ttf": "47d3bf7d3f652d74d47106e38617d9c1",
"assets/assets/fonts/IRANYekanMobileBlack.ttf": "c11210adb359d1c7c430801d155e78e4",
"assets/assets/fonts/IRANYekanMobileBold.ttf": "686e6fe71aee810f04034bd9e5a65a71",
"assets/assets/fonts/IRANYekanMobileExtraBlack.ttf": "c8516516d3a641ab68e9376e9c72af2e",
"assets/assets/fonts/IRANYekanMobileExtraBold.ttf": "94416ca9d72d010295d524f4c1d697d7",
"assets/assets/fonts/IRANYekanMobileLight.ttf": "a93ddbc3f848e74ceddd9534693c13aa",
"assets/assets/fonts/IRANYekanMobileMedium.ttf": "563e0c749bca9ecc57596712d5483c8a",
"assets/assets/fonts/IRANYekanMobileRegular.ttf": "2c58293edeb64bdb41bcc8fa6353515e",
"assets/assets/fonts/IRANYekanMobileThin.ttf": "c31d2fff0d8951a331a1775b4e1b02fc",
"assets/assets/images/add_adress.png": "d1a85d36214f4fff65a1f5319b029c81",
"assets/assets/images/birthday.png": "452da2b6dba2b04dcd10d9855955b06b",
"assets/assets/images/comment.png": "e15147a5c6f5675850a0b088274f6504",
"assets/assets/images/cream.png": "8fb4a9551d98a85386131136ec637d8a",
"assets/assets/images/gift.png": "aa1825afe031f97a9240c25a49389e2e",
"assets/assets/images/home.png": "eeb0a10f302d82997d497e42fbbd8180",
"assets/assets/images/insurance.png": "e5bb8540427467ff203734d497ba43c5",
"assets/assets/images/line.png": "3ae9488a2eee9eca5099d18defa73d81",
"assets/assets/images/location.png": "5296ca87337a69dd964a2c548ade40e8",
"assets/assets/images/logo.png": "6992050b82115671707376d9187add97",
"assets/assets/images/marker.png": "60873ad6935dc6590d01b4c68e5cede8",
"assets/assets/images/person_profile_image.png": "063405cdb2c249b9641668d56b652251",
"assets/assets/images/pharmacy.png": "4f2b4b2648d7fb0a0cb4c8bc291743bd",
"assets/assets/images/placeholder_pharmacy.png": "253e77cb22f40500e6bc74b716c920bd",
"assets/assets/images/previous_orders.png": "5a8692cd304b427fbbaa597ee32a83e2",
"assets/assets/images/product_detail.png": "b7766e1e4103f1ed3f8c47627e22495b",
"assets/assets/images/qr_code.png": "340aaff82c4e191bcad3d3e41541b1ac",
"assets/assets/images/search-logo.png": "3d1ec22ddf45ffcc9231c83d118fc9fa",
"assets/assets/images/search-pharmacy.png": "be2e7616519d328849656df87e4bad10",
"assets/assets/images/tick.png": "be10432d433a428ce9a739b412d83838",
"assets/assets/images/warning.png": "2352b5dea3fd70e48056334a48ed2f71",
"assets/assets/images/wifi.png": "c0b0b88718993fb4628b2f14ffa7f20f",
"assets/assets/svg/accepted_orders.svg": "dd6946d4ab0c1c947fb597af817c81d5",
"assets/assets/svg/address_not_found.svg": "213ed813fc462cbd89c4279afe0938be",
"assets/assets/svg/add_address.svg": "58940bc761d93034e0c1fdb49b59bbcb",
"assets/assets/svg/arrow_down.svg": "8582cabd170635fdb94b6b2dd9e2bd80",
"assets/assets/svg/bin.svg": "610598583b618da0db40ebeee65375cb",
"assets/assets/svg/calendar.svg": "da007f309eadbcae23214c07b6d911f3",
"assets/assets/svg/close.svg": "1d61460bb0eb376e6ddab8ae3ccca6b7",
"assets/assets/svg/empty_address_list.svg": "5df622fda28bfc7b5e1836cbe0001c0f",
"assets/assets/svg/factor.svg": "84d8ed958c83fad162eb3e50285db108",
"assets/assets/svg/feedback.svg": "cd52fb447196e3fae299da4b7f2605c1",
"assets/assets/svg/home_emptyArrow.svg": "760adf444bd475d68d63e5d3a42e3695",
"assets/assets/svg/home_noCurrentOrders.svg": "6a78fec88090e680575e9e0124fd033c",
"assets/assets/svg/icon_play.svg": "de29106ce43787533c089867589c96d3",
"assets/assets/svg/icon_stop.svg": "9d82f42aa2e442b81f13680ecadfe62a",
"assets/assets/svg/icon_support.svg": "67f5e09f91da7ec6a9010f5ba4944aa8",
"assets/assets/svg/icon_video.svg": "3561e6286410206bf2e847e3cfaff4bc",
"assets/assets/svg/icon_voice.svg": "904f349a26f9b64e1254b2b36db0d4d8",
"assets/assets/svg/image_placeholder.svg": "33d1494ec73f606e95496f02ee63ca2e",
"assets/assets/svg/insurance.svg": "e8838772884068fe39c129a3ad1d1abe",
"assets/assets/svg/inviteFriends.svg": "13c7f5e18b1b2db68e82d0bcb1b5eaf0",
"assets/assets/svg/logo_with_text.svg": "fb11207e58a0602aa84c7752dcd0ffe6",
"assets/assets/svg/new_orders.svg": "e08e645a80406910eaf5c59cad712df8",
"assets/assets/svg/no_connection.svg": "6bff75e9e8e5f70798ce13b2953b258c",
"assets/assets/svg/onboard-1.svg": "5555cbe4c438c8638e1da7c4e8298852",
"assets/assets/svg/onboard-2.svg": "2b68c5d864f9e87184a7d20d17f094a3",
"assets/assets/svg/onboard-3.svg": "f3203a28a563ae36a221fd37c5e6ba29",
"assets/assets/svg/order_code_icon.svg": "9e5f521622d11f25cf58b5fb804ee2df",
"assets/assets/svg/order_description.svg": "c784d21e3e33d3199178a218f9a8954a",
"assets/assets/svg/order_name_icon.svg": "1baf80e2fa13fc905e54fb73dccd4ac3",
"assets/assets/svg/payment_success.svg": "c435d7728a569eccdb72a684fca55c0c",
"assets/assets/svg/pencil.svg": "5e75916b4f571aa102d21426ace8687e",
"assets/assets/svg/pharmacy.svg": "467c8acb9de15a6c97e894f3438db01f",
"assets/assets/svg/picture_icon.svg": "b85f7c494eb97aa82e8296d8b3a4e9a2",
"assets/assets/svg/priced_orders.svg": "621baa663e449e8ee1a49334e48143f6",
"assets/assets/svg/research.svg": "9e20c20907e5aa45f68b92c66ebc04ab",
"assets/assets/svg/shipping.svg": "94c40a80dbeddb59ad6311c6acb505e4",
"assets/assets/svg/ticket.svg": "ded8935a6d8721ccf1f6400c834b443c",
"assets/assets/svg/truck.svg": "ccc929ce159a8e9eefab2a8cd51bfd48",
"assets/assets/svg/user_icon.svg": "1586cdff7b2b0240e953a1df202aebe7",
"assets/assets/svg/wallet.svg": "e09acc3849f60f89864e07885748c0c6",
"assets/assets/svg/warning.svg": "b491f0334da7998b3a882203921ca256",
"assets/FontManifest.json": "94a8a35de4fe72c2af2064fbcc2b8c64",
"assets/fonts/MaterialIcons-Regular.otf": "4e6447691c9509f7acdbf8a931a85ca1",
"assets/NOTICES": "00c3eb2fa2d5412c8e6ba54b758366e2",
"assets/packages/cupertino_icons/assets/CupertinoIcons.ttf": "6d342eb68f170c97609e9da345464e5e",
"assets/packages/flutter_sound_web/js/flutter_sound/flutter_sound.js": "aecd83c80bf4faace0bcea4cd47ab307",
"assets/packages/flutter_sound_web/js/flutter_sound/flutter_sound_player.js": "ab009562c726b262f996cb55447ef32a",
"assets/packages/flutter_sound_web/js/flutter_sound/flutter_sound_recorder.js": "f7ac74c4e0fd5cd472d86c3fe93883fc",
"assets/packages/flutter_sound_web/js/howler/howler.core.min.js": "55e0af0319483be8a7371a2cceacf921",
"assets/packages/flutter_sound_web/js/howler/howler.js": "2bba823e6b4d71ea019d81d384672823",
"assets/packages/flutter_sound_web/js/howler/howler.min.js": "0245b64fba989b9e3fd5b253f683d0e4",
"assets/packages/flutter_sound_web/js/howler/howler.spatial.min.js": "28305f7b4898c9b49d523b2e80293ec8",
"assets/packages/wakelock_web/assets/no_sleep.js": "7748a45cd593f33280669b29c2c8919a",
"canvaskit/canvaskit.js": "43fa9e17039a625450b6aba93baf521e",
"canvaskit/canvaskit.wasm": "04ed3c745ff1dee16504be01f9623498",
"canvaskit/profiling/canvaskit.js": "f3bfccc993a1e0bfdd3440af60d99df4",
"canvaskit/profiling/canvaskit.wasm": "a9610cf39260f60fbe7524a785c66101",
"favicon.png": "5dcef449791fa27946b3d35ad8803796",
"icons/Icon-192.png": "ac9a721a12bbc803b44f645561ecb1e1",
"icons/Icon-512.png": "96e752610906ba2a93c65f8abe1645f1",
"index.html": "b40d0de95b8bdc5b164d4417e8d31514",
"/": "b40d0de95b8bdc5b164d4417e8d31514",
"main.dart.js": "91732772483fee79b7bdb01d9888ce19",
"manifest.json": "1a618272165c48e53de6d2ed19b840a7",
"version.json": "6ccf0fbcf59c83c1bc51b424a0c8a333"
};

// The application shell files that are downloaded before a service worker can
// start.
const CORE = [
  "/",
"main.dart.js",
"index.html",
"assets/NOTICES",
"assets/AssetManifest.json",
"assets/FontManifest.json"];
// During install, the TEMP cache is populated with the application shell files.
self.addEventListener("install", (event) => {
  self.skipWaiting();
  return event.waitUntil(
    caches.open(TEMP).then((cache) => {
      return cache.addAll(
        CORE.map((value) => new Request(value, {'cache': 'reload'})));
    })
  );
});

// During activate, the cache is populated with the temp files downloaded in
// install. If this service worker is upgrading from one with a saved
// MANIFEST, then use this to retain unchanged resource files.
self.addEventListener("activate", function(event) {
  return event.waitUntil(async function() {
    try {
      var contentCache = await caches.open(CACHE_NAME);
      var tempCache = await caches.open(TEMP);
      var manifestCache = await caches.open(MANIFEST);
      var manifest = await manifestCache.match('manifest');
      // When there is no prior manifest, clear the entire cache.
      if (!manifest) {
        await caches.delete(CACHE_NAME);
        contentCache = await caches.open(CACHE_NAME);
        for (var request of await tempCache.keys()) {
          var response = await tempCache.match(request);
          await contentCache.put(request, response);
        }
        await caches.delete(TEMP);
        // Save the manifest to make future upgrades efficient.
        await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
        return;
      }
      var oldManifest = await manifest.json();
      var origin = self.location.origin;
      for (var request of await contentCache.keys()) {
        var key = request.url.substring(origin.length + 1);
        if (key == "") {
          key = "/";
        }
        // If a resource from the old manifest is not in the new cache, or if
        // the MD5 sum has changed, delete it. Otherwise the resource is left
        // in the cache and can be reused by the new service worker.
        if (!RESOURCES[key] || RESOURCES[key] != oldManifest[key]) {
          await contentCache.delete(request);
        }
      }
      // Populate the cache with the app shell TEMP files, potentially overwriting
      // cache files preserved above.
      for (var request of await tempCache.keys()) {
        var response = await tempCache.match(request);
        await contentCache.put(request, response);
      }
      await caches.delete(TEMP);
      // Save the manifest to make future upgrades efficient.
      await manifestCache.put('manifest', new Response(JSON.stringify(RESOURCES)));
      return;
    } catch (err) {
      // On an unhandled exception the state of the cache cannot be guaranteed.
      console.error('Failed to upgrade service worker: ' + err);
      await caches.delete(CACHE_NAME);
      await caches.delete(TEMP);
      await caches.delete(MANIFEST);
    }
  }());
});

// The fetch handler redirects requests for RESOURCE files to the service
// worker cache.
self.addEventListener("fetch", (event) => {
  if (event.request.method !== 'GET') {
    return;
  }
  var origin = self.location.origin;
  var key = event.request.url.substring(origin.length + 1);
  // Redirect URLs to the index.html
  if (key.indexOf('?v=') != -1) {
    key = key.split('?v=')[0];
  }
  if (event.request.url == origin || event.request.url.startsWith(origin + '/#') || key == '') {
    key = '/';
  }
  // If the URL is not the RESOURCE list then return to signal that the
  // browser should take over.
  if (!RESOURCES[key]) {
    return;
  }
  // If the URL is the index.html, perform an online-first request.
  if (key == '/') {
    return onlineFirst(event);
  }
  event.respondWith(caches.open(CACHE_NAME)
    .then((cache) =>  {
      return cache.match(event.request).then((response) => {
        // Either respond with the cached resource, or perform a fetch and
        // lazily populate the cache.
        return response || fetch(event.request).then((response) => {
          cache.put(event.request, response.clone());
          return response;
        });
      })
    })
  );
});

self.addEventListener('message', (event) => {
  // SkipWaiting can be used to immediately activate a waiting service worker.
  // This will also require a page refresh triggered by the main worker.
  if (event.data === 'skipWaiting') {
    self.skipWaiting();
    return;
  }
  if (event.data === 'downloadOffline') {
    downloadOffline();
    return;
  }
});

// Download offline will check the RESOURCES for all files not in the cache
// and populate them.
async function downloadOffline() {
  var resources = [];
  var contentCache = await caches.open(CACHE_NAME);
  var currentContent = {};
  for (var request of await contentCache.keys()) {
    var key = request.url.substring(origin.length + 1);
    if (key == "") {
      key = "/";
    }
    currentContent[key] = true;
  }
  for (var resourceKey of Object.keys(RESOURCES)) {
    if (!currentContent[resourceKey]) {
      resources.push(resourceKey);
    }
  }
  return contentCache.addAll(resources);
}

// Attempt to download the resource online before falling back to
// the offline cache.
function onlineFirst(event) {
  return event.respondWith(
    fetch(event.request).then((response) => {
      return caches.open(CACHE_NAME).then((cache) => {
        cache.put(event.request, response.clone());
        return response;
      });
    }).catch((error) => {
      return caches.open(CACHE_NAME).then((cache) => {
        return cache.match(event.request).then((response) => {
          if (response != null) {
            return response;
          }
          throw error;
        });
      });
    })
  );
}
